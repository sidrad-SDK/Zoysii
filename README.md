# Z O Y S I I

A simple game: swipe to delete numbers on a square space.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/xyz.deepdaikon.zoysii/)

## Modes

* Single player
* Multiplayer (vs CPU)
* Levels

## Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/HomePage.png" height="320">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/LevelGame.png" height="320">
<img src ="fastlane/metadata/android/en-US/images/phoneScreenshots/Multiplayer.png" height="320">

## Rules

1- You are the red tile on a square board.

2- Swipe horizzontally or vertically to move.

3- When you move you reduce tiles value in the direction you are going.

3a- The amount of this reduction is equal to your starting point tile value.

3b- But if the value of a tile would be equal to 1 or 2, there will be an increase instead of a decrease.

3c- Negative numbers become positive.

3d- If the value of a tile becomes equal to zero, starting tile value becomes zero too. Tiles have been "Deleted".

4- Deleted tiles cause an increase of your points.

5- The aim in single and multiplayer modes is to delete almost every tile (~75-90%) while trying to make the most points.

6- In multiplayer matches a player can win by deleting opponent's tile.