import 'package:flutter/material.dart';

import 'package:zoysii/src/screens/settings/resources/settings_class.dart';

// Zoysii app version
const String appVersion = '1.2.0';

// Global font size dimension
const double kTextFontSize = 20;

// Game settings
Settings settings = Settings();

// List of match results structured as below:
// [initDate, endDate, match.id, sideLength, moves, points,
// minBoardInt, maxBoardInt, biggestLow]
List<List<int>> results = [];

// Next level to win
// nextLevel[0] == zone, nextLevel[1] == level
List<int> nextLevel = List(2);

// Virtual gamepad position
Offset kGamepadOffset;