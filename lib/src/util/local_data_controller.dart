import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/settings/resources/settings_class.dart';

// Import all data from shared preferences
void loadStoredData() {
  _loadResults().then((x) => results = x);
  _loadSettings();
  _loadNextLevel();
}

// Save results in shared preferences
void saveResults(int initDate, int endDate, int matchId, int sideLength,
    int moves, int points, minBoardInt, maxBoardInt, biggestLow) async {
  results.add([
    initDate,
    endDate,
    matchId,
    sideLength,
    moves,
    points,
    minBoardInt,
    maxBoardInt,
    biggestLow
  ]);
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('results', jsonEncode(results));
}

// Restore results
Future<List<List<int>>> _loadResults() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  List<List<int>> _results = List<List<int>>();
  try {
    List<dynamic> stringList =
        (jsonDecode(prefs.getString('results')) as List<dynamic>)
            .cast<List<dynamic>>();
    stringList.forEach((x) => _results.add(x.cast<int>()));
  } on NoSuchMethodError {
    // Empty list
  }
  return Future<List<List<int>>>(() => _results);
}

// Save app settings in shared preferences
void saveSettings() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('settings', jsonEncode(settings.toJson()));
}

void saveGamepadOffset() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setDouble('gamepadOffsetX', kGamepadOffset.dx);
  prefs.setDouble('gamepadOffsetY', kGamepadOffset.dy);
}

// Restore app settings
void _loadSettings() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  settings = Settings.fromJson(jsonDecode(prefs.getString('settings') ?? '{}'));
  kGamepadOffset = Offset(prefs.getDouble('gamepadOffsetX') ?? 0,
      prefs.getDouble('gamepadOffsetY') ?? 0);
}

// Save next level to play in shared preferences
void saveNextLevel() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt('lastWorldSolved', nextLevel[0]);
  prefs.setInt('lastLevelSolved', nextLevel[1]);
}

// Restore next level to play
void _loadNextLevel() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  nextLevel[0] = prefs.getInt('lastWorldSolved') ?? 0;
  nextLevel[1] = prefs.getInt('lastLevelSolved') ?? 0;
}
