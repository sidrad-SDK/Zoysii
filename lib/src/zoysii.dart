import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:zoysii/src/screens/home/home_page.dart';
import 'package:zoysii/src/util/local_data_controller.dart';

class Zoysii extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    loadStoredData();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      title: 'Zoysii',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        primaryColor: Colors.blueGrey[800],
      ),
      home: HomePage(),
    );
  }
}
