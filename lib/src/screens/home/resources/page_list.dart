import 'package:flutter/material.dart';

import 'package:zoysii/src/screens/game/game_page.dart';
import 'package:zoysii/src/screens/info/info_page.dart';
import 'package:zoysii/src/screens/levels/levels_page.dart';
import 'package:zoysii/src/screens/ranking/ranking_page.dart';
import 'package:zoysii/src/screens/rules/rules_page.dart';
import 'package:zoysii/src/screens/settings/settings_page.dart';

// List of available pages displayed in home page
final List<Map<String, dynamic>> pageList = [
  {
    'title': 'Quick match',
    'subtitle': 'One player mode',
    'page': GamePage(),
    'icon': Icon(Icons.play_arrow),
    'gameMode': 1,
  },
  {
    'title': 'Versus CPU',
    'subtitle': 'Play against CPU',
    'page': GamePage(),
    'icon': Icon(Icons.label_important),
    'gameMode': 2,
  },
  {
    'title': 'Levels',
    'subtitle': 'Solve and complete all levels',
    'page': LevelsPage(),
    'icon': Icon(Icons.toys),
  },
  {
    'divider': 'Play',
  },
  {
    'title': 'Ranking',
    'subtitle': 'Watch your results',
    'page': RankingPage(),
    'icon': Icon(Icons.equalizer),
  },
  {
    'title': 'Rules',
    'subtitle': 'How to play',
    'page': RulesPage(),
    'icon': Icon(Icons.school),
  },
  {
    'divider': 'Play',
  },
  {
    'title': 'Settings',
    'subtitle': 'Change the settings',
    'page': SettingsPage(),
    'icon': Icon(Icons.settings),
  },
  {
    'title': 'Info',
    'subtitle': 'About this game',
    'page': InfoPage(),
    'icon': Icon(Icons.info),
  },
];
