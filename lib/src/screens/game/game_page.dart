import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/resources/levels.dart';
import 'package:zoysii/src/screens/game/util/match.dart';
import 'package:zoysii/src/screens/game/util/player.dart';
import 'package:zoysii/src/screens/game/util/random.dart';
import 'package:zoysii/src/screens/game/widgets/gamepad.dart';
import 'package:zoysii/src/screens/game/widgets/percent_indicator.dart';
import 'package:zoysii/src/screens/settings/resources/option_values.dart';
import 'package:zoysii/src/util/number_notation_converter.dart';
import 'package:zoysii/src/widgets/toast.dart';

Match match;

// 0: levels, 1: one player, 2: two players
int mode;

// level[0]: zone, level[1]: level
List<int> level;

// Numeral system used during the match
int enabledNumeralSystem;

class GamePage extends StatefulWidget {
  static _GamePageState of(BuildContext context) =>
      context.ancestorStateOfType(const TypeMatcher<_GamePageState>());

  @override
  _GamePageState createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  _GamePageState();
  Timer timer;

  @override
  void initState() {
    match = Match();
    enabledNumeralSystem = mode != 0 ? settings.numeralSystem : 0;
    if (mode == 2) {
      timer = Timer.periodic(
          Duration(seconds: 1),
          (Timer t) => setState(() {
                if (!match.isPause && (match.playerOneWon == null)) {
                  playerTwo.cpuMove();
                  _endDialog(match.someoneWon());
                }
              }));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String pageTitle;
    String textBar;
    if (mode == 0) {
      pageTitle = match.levelName;
      textBar = 'Remaining moves: ${match.maxMoves - playerOne.moves}';
    } else {
      pageTitle = '${match.toBeDeleted} remaining';
      textBar = playerOne.points.toString() +
          (mode == 2 ? ' - ${playerTwo.points}' : '');
    }

    // Variables to manage gestures input
    double initialX, initialY, distanceX, distanceY = 0.0;

    return Scaffold(
      appBar: AppBar(
          title: Text(pageTitle),
          backgroundColor: Colors.lightBlue[900],
          actions: <Widget>[
            if (mode != 0 && nextLevel[0] > 0)
              PopupMenuButton<int>(
                icon: Icon(Icons.translate),
                tooltip: 'Numeral system',
                onSelected: (int selectedSystem) {
                  setState(() {
                    enabledNumeralSystem = selectedSystem;
                  });
                },
                itemBuilder: (BuildContext context) {
                  List<PopupMenuEntry<int>> popupEntries = [];
                  numberRepresentation.forEach((systemNum, systemText) {
                    if (systemNum <= nextLevel[0])
                      popupEntries.add(PopupMenuItem<int>(
                          value: systemNum, child: Text(systemText)));
                  });
                  return popupEntries;
                },
              ),
            if (mode == 0)
              IconButton(
                icon: Icon(Icons.translate),
                onPressed: () {
                  setState(() {
                    enabledNumeralSystem =
                        enabledNumeralSystem == level[0] + 1 ? 0 : level[0] + 1;
                  });
                  Toast.show(
                      "Numeral system enabled: ${numberRepresentation[enabledNumeralSystem]}",
                      context);
                },
              ),
            IconButton(
              icon: Icon(Icons.repeat),
              onPressed: () {
                _repeatDialog(context);
                setState(() {});
              },
            ),
            IconButton(
              icon: Icon(Icons.pause),
              onPressed: () {
                _pauseDialog(context);
                setState(() {});
              },
            ),
          ]),
      body: Stack(children: <Widget>[
        Center(
          child: GestureDetector(
            onTapDown: settings.inputMethod == 0
                ? (TapDownDetails details) {
                    _gestureOnTapController();
                  }
                : null,
            onHorizontalDragStart: settings.inputMethod == 0
                ? (DragStartDetails details) {
                    initialX = details.globalPosition.dx;
                  }
                : null,
            onHorizontalDragUpdate: settings.inputMethod == 0
                ? (DragUpdateDetails details) {
                    if (initialX != null)
                      distanceX = details.globalPosition.dx - initialX;
                  }
                : null,
            onHorizontalDragEnd: settings.inputMethod == 0
                ? (DragEndDetails details) {
                    initialX = 0.0;
                    if (distanceX != null) {
                      inputController(0, distanceX);
                    }
                  }
                : null,
            onVerticalDragStart: settings.inputMethod == 0
                ? (DragStartDetails details) {
                    initialY = details.globalPosition.dy;
                  }
                : null,
            onVerticalDragUpdate: settings.inputMethod == 0
                ? (DragUpdateDetails details) {
                    if (initialY != null)
                      distanceY = details.globalPosition.dy - initialY;
                  }
                : null,
            onVerticalDragEnd: settings.inputMethod == 0
                ? (DragEndDetails details) {
                    initialY = 0.0;
                    if (distanceY != null) {
                      inputController(1, distanceY);
                    }
                  }
                : null,
            child: GridView.count(
              physics: const NeverScrollableScrollPhysics(),
              crossAxisCount: match.sideLength,
              children: _gridComposer(),
            ),
          ),
        ),
        if (settings.inputMethod == 1) FloatingGamepad(),
      ]),
      bottomNavigationBar: LinearPercentIndicator(
        width: MediaQuery.of(context).size.width,
        center: Text(
          textBar,
          style: TextStyle(fontSize: kTextFontSize - 2, color: Colors.white),
        ),
      ),
    );
  }

  // Manage horizontal and vertical drag input
  void inputController(int direction, double distance) {
    playerOne.doMove(direction, distance);
    setState(() {});
    _endDialog(match.someoneWon());
  }

  // Manage on tap input (to be implemented)
  void _gestureOnTapController() {}

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  // End match dialog
  void _endDialog(bool win) {
    if (win == null) return;
    match.isPause = true;
    String titleText;
    String subText;
    if (win) {
      titleText = 'You won';
      subText = 'Congratulations!\n';
      switch (mode) {
        case 0:
          subText +=
              'You completed the level by doing ${playerOne.moves} moves';
          break;
        case 1:
          subText +=
              'You did ${playerOne.points} points in ${playerOne.moves} moves';
          break;
        case 2:
          if (match.toBeDeleted <= 0)
            subText +=
                'You won by doing more points than your opponent: ${playerOne.points} vs ${playerTwo.points}';
          if (match.toBeDeleted > 0)
            subText += 'You won because you deleted your opponent';
          break;
      }
    } else {
      titleText = 'You lost';
      if (mode == 0)
        subText = 'You did not completed the level in ${match.maxMoves} moves.';
      else
        subText = match.toBeDeleted <= 0
            ? 'Your opponent won by doing more points than you: ${playerOne.points} vs ${playerTwo.points}.'
            : 'Your opponent deleted you.';
    }
    subText += '\n\nWhat do you want to do?';

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () => null,
            child: AlertDialog(
              title: Text(titleText),
              content: Text(subText),
              actions: <Widget>[
                FlatButton(
                  child: Text('Exit'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.pop(context);
                  },
                ),
                FlatButton(
                  child: Text('Restart match'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    match.id = currentSeed;
                    setState(() {
                      match.init();
                    });
                  },
                ),
                if (mode == 0 &&
                    match.selectedLevel[1] == 6 &&
                    match.playerOneWon)
                  FlatButton(
                    child: Text('Next Zone'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.pop(context);
                      if (nextLevel[0] == match.selectedLevel[0] + 1 &&
                          nextLevel[1] == 0)
                        Toast.show(
                            "Congratulations!"
                            "\nYou unlocked ${numberRepresentation[nextLevel[0]]} numeral system."
                            "\nEnable it in settings.",
                            context);
                    },
                  ),
                if ((mode != 0 && match.playerOneWon != null) ||
                    (mode == 0 &&
                        match.selectedLevel[1] != 6 &&
                        match.playerOneWon))
                  FlatButton(
                    child: Text('New match'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      if (mode != 0) {
                        match.id = -1;
                      } else {
                        if (match.selectedLevel[1] < 6) {
                          ++match.selectedLevel[1];
                        } else if (match.selectedLevel[0] < zones.length - 1) {
                          ++match.selectedLevel[0];
                          match.selectedLevel[1] = 0;
                        }
                      }
                      setState(() {
                        match.init();
                      });
                    },
                  ),
              ],
            ));
      },
    );
  }

  // Pause match dialog
  void _pauseDialog(BuildContext context) {
    match.isPause = true;

    String subText = 'Remaining tiles: ';
    subText += mode != 0
        ? '${match.toBeDeleted}'
        : '${match.board.length - match.board.where((element) => element == 0).length}';
    subText += '\n\n' +
        (mode == 2 ? '- You:\n' : '') +
        'Points: ${playerOne.points}\nMoves: ${playerOne.moves}';
    subText += mode == 2
        ? '\n\n- Your opponent:\nPoints: ${playerTwo.points}\nMoves: ${playerTwo.moves}'
        : '';

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () => null,
            child: AlertDialog(
              title: Text('Pause'),
              content: Text(subText),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    match.isPause = false;
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
      },
    );
  }

  // Repeat match dialog
  void _repeatDialog(BuildContext context) {
    match.isPause = true;

    TextEditingController _textFieldController =
        TextEditingController(text: currentSeed.toString());

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          if (mode != 0) {
            return WillPopScope(
                onWillPop: () => null,
                child: AlertDialog(
                  title: Text("New match"),
                  content: TextField(
                    controller: _textFieldController,
                    decoration: InputDecoration(helperText: "Match ID"),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        match.isPause = false;
                        Navigator.of(context).pop();
                      },
                    ),
                    FlatButton(
                      child: Text('New match'),
                      onPressed: () {
                        Navigator.of(context).pop();
                        int matchIdInput = int.parse(_textFieldController.text);
                        match.id = matchIdInput == match.id
                            ? -1
                            : match.id = matchIdInput;
                        setState(() {
                          match.init();
                        });
                      },
                    ),
                    FlatButton(
                      child: Text('Restart'),
                      onPressed: () {
                        Navigator.of(context).pop();
                        setState(() {
                          match.init();
                        });
                      },
                    ),
                  ],
                ));
          } else {
            return WillPopScope(
              onWillPop: () => null,
              child: AlertDialog(
                title: Text("Restart match"),
                content: Text("Do you want to restart this match?"),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Cancel'),
                    onPressed: () {
                      match.isPause = false;
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('Restart'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      match.id = -1;
                      setState(() {
                        match.init();
                      });
                    },
                  ),
                ],
              ),
            );
          }
        });
  }
}

// Generate game grid and return list of tile widgets
List<Widget> _gridComposer() =>
    List.generate(match.sideLength * match.sideLength, (index) {
      Color color;
      double fontSize = 32;
      if (index == playerOne.position &&
          ((index != playerTwo?.position && mode == 2) || mode != 2)) {
        color = Colors.red[400];
      }
      if (index == playerOne.position &&
          index == playerTwo?.position &&
          mode == 2) {
        color = Colors.green[400];
      }
      if (index != playerOne.position &&
          index == playerTwo?.position &&
          mode == 2) {
        color = Colors.blue[400];
      }
      if ((index != playerTwo?.position && index != playerOne.position) ||
          (index == playerTwo?.position &&
              index != playerOne.position &&
              mode != 2)) {
        fontSize = 24;
        color = Colors.black;
      }

      return Center(
        child: FittedBox(
            fit: BoxFit.fitWidth,
            child: Text(
              intToSelectedNotation(match.board[index], enabledNumeralSystem),
              style: TextStyle(
                fontSize: fontSize,
                color: color,
                fontFamily: notationFontMap[enabledNumeralSystem],
              ),
            )),
      );
    });
