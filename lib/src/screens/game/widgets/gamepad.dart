import 'package:flutter/material.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/game/game_page.dart';
import 'package:zoysii/src/util/local_data_controller.dart';

// Floating virtual gamepad used to move the pointer
class FloatingGamepad extends StatefulWidget {
  @override
  _FloatingGamepadState createState() => _FloatingGamepadState();
}

// Map direction-button icon
Map<String, dynamic> arrowIconMap = {
  'up': Icon(Icons.keyboard_arrow_up),
  'down': Icon(Icons.keyboard_arrow_down),
  'left': Icon(Icons.keyboard_arrow_left),
  'right': Icon(Icons.keyboard_arrow_right),
};

// Floating gamepad widget
class _FloatingGamepadState extends State<FloatingGamepad> {
  Color _buttonColor = Colors.blueGrey[500];
  Offset _offset = kGamepadOffset;
  Widget build(BuildContext context) {
    return Positioned(
        right: _offset.dx,
        bottom: _offset.dy,
        child: Column(
          children: [
            Row(
              children: [
                _separator(),
                _arrowButton('up'),
                _separator(),
              ],
            ),
            Row(
              children: [
                _arrowButton('left'),
                if (settings.gamepadShape == 0)
                  _arrowButton('center')
                else
                  _arrowButton('down'),
                _arrowButton('right'),
              ],
            ),
            if (settings.gamepadShape == 0)
              Row(
                children: [
                  _separator(),
                  _arrowButton('down'),
                  _separator(),
                ],
              ),
          ],
        ));
  }

  // Gamepad button widget
  Widget _arrowButton(String direction) {
    return GestureDetector(
        // Edit widget position by moving its center
        onPanUpdate: (details) {
          setState(() {
            _offset = Offset(
                _offset.dx - details.delta.dx, _offset.dy - details.delta.dy);
          });
        },
        child: direction != 'center'
            ? IconButton(
                icon: arrowIconMap[direction],
                iconSize: settings.gamepadSize,
                color: _buttonColor,
                onPressed: () {
                  switch (direction) {
                    case 'up':
                      _upInput();
                      break;
                    case 'down':
                      _downInput();
                      break;
                    case 'left':
                      _leftInput();
                      break;
                    case 'right':
                      _rightInput();
                      break;
                    default:
                      break;
                  }
                },
              )
            : Container(
                width: settings.gamepadSize,
                height: settings.gamepadSize,
                child: Icon(
                  Icons.fiber_manual_record,
                  size: settings.gamepadSize / 3,
                )));
  }

  // White space in gamepad
  Widget _separator() =>
      Container(width: settings.gamepadSize, height: settings.gamepadSize);

  // Manage up direction input
  void _upInput() {
    GamePage.of(context).inputController(1, -1);
    _saveOffset(_offset);
  }

  // Manage down direction input
  void _downInput() {
    GamePage.of(context).inputController(1, 1);
    _saveOffset(_offset);
  }

  // Manage left direction input
  void _leftInput() {
    GamePage.of(context).inputController(0, -1);
    _saveOffset(_offset);
  }

  // Manage right direction input
  void _rightInput() {
    GamePage.of(context).inputController(0, 1);
    _saveOffset(_offset);
  }

  // Save widget position to be used in next matches
  void _saveOffset(Offset newOffset) {
    if (newOffset != kGamepadOffset) {
      kGamepadOffset = newOffset;
      saveGamepadOffset();
    }
  }
}
