// Class used to store settings data
class Settings {
  // Grid side length
  int sideLength;

  // Selected input method
  int inputMethod;

  // Virtual gamepad dimension
  double gamepadSize;

  // Virtual gamepad shape
  int gamepadShape;

  // Board range values
  int maxBoardInt;
  int minBoardInt;

  // Biggest "low number" for rule 3b
  int biggestLow;

  // Numeral notation
  int numeralSystem;

  Settings()
      : this.sideLength = 6,
        this.inputMethod = 0,
        this.gamepadSize = 50,
        this.gamepadShape = 0,
        this.minBoardInt = 1,
        this.maxBoardInt = 19,
        this.biggestLow = 2,
        this.numeralSystem = 0;

  Settings.fromJson(Map<String, dynamic> json)
      : this.sideLength = json['sideLength'] ?? 6,
        this.inputMethod = json['inputMethod'] ?? 0,
        this.gamepadSize = json['gamepadSize'] ?? 50,
        this.gamepadShape = json['gamepadShape'] ?? 0,
        this.minBoardInt = json['minBoardInt'] ?? 1,
        this.maxBoardInt = json['maxBoardInt'] ?? 19,
        this.biggestLow = json['biggestLow'] ?? 2,
        this.numeralSystem = json['numeralSystem'] ?? 0;

  Map<String, dynamic> toJson() => {
        'sideLength': sideLength,
        'inputMethod': inputMethod,
        'gamepadSize': gamepadSize,
        'gamepadShape': gamepadShape,
        'minBoardInt': minBoardInt,
        'maxBoardInt': maxBoardInt,
        'biggestLow': biggestLow,
        'numeralSystem': numeralSystem,
      };
}
