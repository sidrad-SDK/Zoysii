import 'package:flutter/material.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/settings/resources/option_values.dart';
import 'package:zoysii/src/screens/settings/settings_page.dart';

// List of available options displayed in settings page
class OptionList extends StatelessWidget {
  Widget build(BuildContext context) => ListView(children: <Widget>[
        SettingsPage.of(context).simpleTile('Default numeral system',
            'Select numeral notation used by default'),
        Divider(color: Colors.black45),
        SettingsPage.of(context).dropDownTile(
          'sideLength',
          'Grid dimension',
          'Size of the game board',
          settings.sideLength,
          gridSizes,
        ),
        SettingsPage.of(context).rangeTile('Range of values on start'),
        SettingsPage.of(context).dropDownTile(
          'biggestLow',
          'Biggest "Low number"',
          'Change rule 3b definition of "low number"'
              '\n(It does not affect levels mode)',
          settings.biggestLow,
          lowNumberList,
        ),
        Divider(color: Colors.black45),
        SettingsPage.of(context).dropDownTile(
          'inputMethod',
          'Input method',
          'Method used to move',
          settings.inputMethod,
          <int>[0, 1],
          mapText: inputMethods,
        ),
        if (settings.inputMethod == 1)
          SettingsPage.of(context).dropDownTile(
            'gamepadSize',
            'Gamepad size:',
            'Virtual gamepad dimension',
            settings.gamepadSize.toInt(),
            gamepadSizes.keys.toList(),
            mapText: gamepadSizes,
          ),
        if (settings.inputMethod == 1)
          SettingsPage.of(context).dropDownTile(
            'gamepadShape',
            'Gamepad shape:',
            'Select gamepad shape',
            settings.gamepadShape,
            gamepadShape.keys.toList(),
            mapText: gamepadShape,
          ),
      ]);
}

// This function updates the right variables for each option
updateVariables(String element, int newValue) {
  switch (element) {
    case 'sideLength':
      settings.sideLength = newValue;
      break;
    case 'biggestLow':
      settings.biggestLow = newValue;
      break;
    case 'inputMethod':
      settings.inputMethod = newValue;
      break;
    case 'gamepadShape':
      settings.gamepadShape = newValue;
      kGamepadOffset = Offset.zero;
      break;
    case 'gamepadSize':
      settings.gamepadSize = newValue.toDouble();
      kGamepadOffset = Offset.zero;
      break;
    default:
      break;
  }
}
