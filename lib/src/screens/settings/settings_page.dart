import 'package:flutter/material.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/util/local_data_controller.dart';
import 'package:zoysii/src/screens/settings/resources/option_list.dart';
import 'package:zoysii/src/screens/settings/resources/option_values.dart';
import 'package:zoysii/src/screens/settings/resources/settings_class.dart';
import 'package:zoysii/src/widgets/toast.dart';

class SettingsPage extends StatefulWidget {
  static _SettingsPageState of(BuildContext context) =>
      context.ancestorStateOfType(const TypeMatcher<_SettingsPageState>());

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  RangeValues _values = RangeValues(
      settings.minBoardInt.toDouble(), settings.maxBoardInt.toDouble());
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
          title: Text('S E T T I N G S'),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings_backup_restore),
              onPressed: () {
                _restoreSettingsDialog();
              },
            ),
          ]),
      body: OptionList());

  // Dialog used to restore default settings
  _restoreSettingsDialog() => showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            title: Text('Restore default settings?'),
            content: Text(
                'Are you sure you wish to delete your settings and restore default ones?'),
            actions: <Widget>[
              FlatButton(
                child: Text('Yes'),
                onPressed: () {
                  settings = Settings();
                  saveSettings();
                  _values = RangeValues(settings.minBoardInt.toDouble(),
                      settings.maxBoardInt.toDouble());
                  setState(() {});
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('No'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ));

  // Returns a tile widget for options that use drop down button
  Widget dropDownTile(String element, String title, String subtitle, int value,
          List<int> list,
          {Map<int, String> mapText}) =>
      ListTile(
          title: Text(
            title,
            style: TextStyle(
              fontSize: kTextFontSize,
            ),
          ),
          subtitle: Text(subtitle),
          trailing: DropdownButton<int>(
            value: value,
            onChanged: (int newValue) {
              updateVariables(element, newValue);
              setState(() {
                saveSettings();
              });
            },
            items: list
                .map<DropdownMenuItem<int>>((int value) =>
                    DropdownMenuItem<int>(
                      value: value,
                      child: Text(
                          mapText != null ? mapText[value] : value.toString()),
                    ))
                .toList(),
          ));

  // Returns an expansion tile widget for options that use range slider
  Widget rangeTile(String title) => ExpansionTile(
          title: Text(
            title,
            style: TextStyle(
              fontSize: kTextFontSize,
            ),
          ),
          trailing: Text(
            'from ${settings.minBoardInt} to ${settings.maxBoardInt}',
            style: TextStyle(
              fontSize: kTextFontSize - 4,
            ),
          ),
          initiallyExpanded: false,
          children: <Widget>[
            RangeSlider(
                values: _values,
                min: 0,
                max: 50,
                onChanged: (RangeValues values) {
                  setState(() {
                    if (values.end - values.start >= 15) {
                      _values = RangeValues(values.start.round().toDouble(),
                          values.end.round().toDouble());
                    } else {
                      if (_values.start == values.start)
                        _values =
                            RangeValues(_values.start, _values.start + 15);
                      else
                        _values = RangeValues(_values.end - 15, _values.end);
                    }
                    settings.minBoardInt = _values.start.round();
                    settings.maxBoardInt = _values.end.round();
                    saveSettings();
                  });
                })
          ]);

  // Returns a list tile. Used only for numeral system option, for now
  Widget simpleTile(String title, String subtitle) => ListTile(
      title: Text(
        title,
        style: TextStyle(
          fontSize: kTextFontSize,
        ),
      ),
      trailing: Text(
        numberRepresentation[settings.numeralSystem],
        style: TextStyle(
          fontSize: kTextFontSize - 4,
        ),
      ),
      subtitle: Text(subtitle),
      onTap: () => numeralSystemListDialog());

  // Dialog that shows a radio list to select numeral system
  numeralSystemListDialog() => showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            title: Text('Select a Numeral System'),
            contentPadding: EdgeInsets.fromLTRB(0, 20, 0, 20),
            content: SingleChildScrollView(
              child: Column(
                  children: List.generate(
                      numberRepresentation.length,
                      (index) => RadioListTile<int>(
                            title: Text(numberRepresentation[index] +
                                (index > nextLevel[0] ? ' (locked)' : '')),
                            value: index,
                            groupValue: settings.numeralSystem,
                            onChanged: (int value) {
                              if (index > nextLevel[0])
                                Toast.show(
                                    "You have to complete Zone $index in levels mode to unlock this feature.",
                                    context);
                              else {
                                setState(() {
                                  settings.numeralSystem = value;
                                });
                                saveSettings();
                                Navigator.of(context).pop();
                              }
                            },
                          ))),
            ),
          ));
}
