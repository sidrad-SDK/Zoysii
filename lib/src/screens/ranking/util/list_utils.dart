import 'package:zoysii/src/resources/global_variables.dart';

// Sorter type
// 1: order by moves, 2: order by points
int sorter = 1;

// Filter type. It equals to side length to filter
int filter;

// List of results displayed in ranking page
List<List<int>> shownResults = [];

// Sort ranking list
void sortList(int element) {
  shownResults.sort((n0, n1) =>
      (element == 2 ? -1 : 1) * (n0[element + 3] - n1[element + 3]));
}

// Filter ranking list
void filterList(int size) {
  shownResults =
      results.where((result) => result[3] == size && result[2] >= 0).toList();
}
