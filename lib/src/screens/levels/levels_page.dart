import 'package:flutter/material.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/resources/levels.dart';
import 'package:zoysii/src/screens/game/game_page.dart';
import 'package:zoysii/src/util/number_notation_converter.dart';
import 'package:zoysii/src/util/utils.dart';

class LevelsPage extends StatefulWidget {
  @override
  _LevelsPageState createState() => _LevelsPageState();
}

class _LevelsPageState extends State<LevelsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('L E V E L S'),
        centerTitle: true,
        backgroundColor: Colors.green[800],
      ),
      body: ListView.builder(
          itemCount: nextLevel[0] + 1,
          itemBuilder: (BuildContext context, int index) {
            Color buttonColor;
            if (index < zones.length)
              return ExpansionTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.indigoAccent[300],
                  child: Text(
                    intToSelectedNotation(index + 1, index + 1),
                    style: TextStyle(
                      fontFamily: notationFontMap[index + 1],
                      fontSize: kTextFontSize,
                      color: Colors.white,
                    ),
                  ),
                ),
                title: Text(
                  'Zone ${intToRoman(index + 1)} - ${zones[index]}',
                  style: TextStyle(
                    fontSize: kTextFontSize - 2,
                  ),
                ),
                initiallyExpanded: index == nextLevel[0],
                children: <Widget>[
                  Row(
                    children:
                        (levels.where((level) => level['level'][0] == index))
                            .map((element) {
                      buttonColor = listEquals(element['level'], nextLevel)
                          ? Colors.blue[300]
                          : buttonColor = Colors.green[300];
                      return Expanded(
                        child: FlatButton(
                          child: Text((element['level'][1] + 1).toString()),
                          color: buttonColor,
                          disabledColor: Colors.grey[350],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(0.0)),
                          onPressed: (index < nextLevel[0] ||
                                  element['level'][1] <= nextLevel[1])
                              ? () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) {
                                      mode = 0;
                                      level = element['level'];
                                      return GamePage();
                                    }),
                                  );
                                }
                              : null,
                        ),
                      );
                    }).toList(),
                  ),
                ],
              );
            else
              return ListTile(
                contentPadding: EdgeInsets.fromLTRB(15, 20, 0, 0),
                leading: CircleAvatar(
                  backgroundColor: Colors.green[900],
                  child: Text(
                    '^-^',
                    style: TextStyle(
                      fontSize: kTextFontSize,
                      color: Colors.white,
                    ),
                  ),
                ),
                title: Text(
                  'Congratulations!\nYou won every available level!',
                  style: TextStyle(
                    fontSize: kTextFontSize - 2,
                  ),
                ),
                subtitle: Text(
                  'Next Zone will be released soon.',
                  style: TextStyle(
                    fontSize: kTextFontSize - 4,
                  ),
                ),
              );
          }),
    );
  }
}
