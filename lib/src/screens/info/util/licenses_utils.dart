import 'package:zoysii/src/resources/third_party_licenses.dart';

// Return a string that contains all third party licences
String licensesText() {
  String licensesText = '';
  licenses.forEach((license) =>
  licensesText += '• ${license['lib']}\n\n${license['text']}\n\n\n\n');
  return licensesText;
}